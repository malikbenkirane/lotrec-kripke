import sys

model_filename = 'model0'
xml_in = 'logic.xml'
xml_out = '{}.xml'.format(model_filename)

if len(sys.argv) >= 3:
    xml_in = sys.argv[1]
    model_filename = sys.argv[2]

if len(sys.argv) >= 4:
    xml_out = sys.argv[3]

f = open(model_filename, 'r')

world = ''
rel = {}
rels = []
formulas = []
worlds = []
nodes = []

def xml_createNewNode(w):
    return """<action>
    <action-name>createNewNode</action-name>
    <parameter>{}</parameter>
</action>""".format(w)

def xml_link(w1, w2,r):
    return """<action>
    <action-name>link</action-name>
    <parameter>{}</parameter>
    <parameter>{}</parameter>
    <parameter>{}</parameter>
</action>""".format(w1,w2,r)

def xml_add(w, p):
    return """<action>
    <action-name>add</action-name>
    <parameter>{}</parameter>
    <parameter>{}</parameter>
</action>""".format(w,p)

for line in f:

    tokens = line.split(' ')

    if tokens[0] == 'world':
        world = tokens[1].strip()

    elif tokens[0] == 'rel':
        if world == '':
            print("expecting word first")
            break
        if len(tokens) - 1 < 2:
            print("rel expects agent and at least one word")
        rel = {
            'name': tokens[1],
            'worlds': [name.strip() for name in tokens[2:]]
        }
        rels.append(rel)

    elif line.strip() == '':
        if world == '':
            continue
        print('{} ok'.format(world))
        worlds.append({
            'name': world,
            'rels': rels,
            'props': formulas,
        })
        nodes.append(world)
        world = ''
        rels = []
        formulas = []

    else: # everything else is a formula
        formulas.append(line.strip())


if world != '':
    print('{} ok'.format(world))
    worlds.append({
        'name': world,
        'rels': rels,
        'props': formulas,
    })
    nodes.append(world)
    world = ''
    rels = []
    formulas = []

xml = ''

for node in nodes:
    xml += xml_createNewNode(node)

for world in worlds:
    for rel in world['rels']:
        for rel_world in rel['worlds']:
            xml += xml_link(world['name'],rel_world,rel['name'])
    for p in world['props']:
        xml += xml_add(world['name'],p)

import xml.etree.ElementTree as ET

model_xml = ET.fromstring("""<rule>
    <rule-name>ExampleOfModelAndFormula</rule-name>
    {}
    <rule-comment/>
</rule>""".format(xml))

xml = ET.parse(xml_in)
root = xml.getroot()

for i, child in enumerate(root):
    if child.tag == 'rule':
        for rule_child in child:
            if rule_child.tag == 'rule-name':
                if rule_child.text == 'ExampleOfModelAndFormula':
                    root.remove(child)
                    root.insert(i, model_xml)
                    break
        break

xml.write(xml_out)