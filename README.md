# lotrec-kripke

## Usage

**Step 1**: save logic from LoTREC 2.0

**Step 2**: write a kripke model (for instance see **model0** or look at the syntax below)

**Step 3**: run

```sh
python3 update_model.py [*LogicIN.xml* *model* [*LogicOUT.xml*]]
```

By default

- `*LogicIN.xml*` is `logic.xml`
- `*model*` is `model0`
- `*LogicOUT.xml*` is `*model*.xml`

**Step 4**: open the logic from LoTREC 2.0

## Kripke simplified syntax

```other
MODEL := WORLDS

WORLDS := WORLD \n WORLDS

WORLD := world WORLD_NAME
rel AGENT_NAME WORLD_LIST
FORMULA_LIST

WORLD_NAME := w1 | ...

AGENT_NAME := R | ...

WORLD_LIST := WORLD_NAME WORLD_LIST

FORMULA_LIST := LOTREC_FORMULA \n FORMULA_LIST
```

- Non captial names are symbols
- `\n` is the new line character
- Almost BNF syntax description (if you can't figure it out give look at model0)

## Feedbacks

- issues are welcome
- merge requests are welcome too

Just enjoy your model testing with LoTREC now !
